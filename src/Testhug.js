import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
    Grid,
    Card,
    CardContent,
    Button,
} from '@mui/material';


const Test = () => {
    const [products, setProducts] = useState([]);
    const [stock, setStock] = useState([]);
    const [cart, setCart] = useState([]);

    useEffect(() => {
        GetMaster();
    }, []);

    const GetMaster = async () => {
        try {
            const response = await axios.get('https://localhost:7116/api/MasterProduct', {
            });
            setProducts(response.data)
            setStock(response.data)
            console.log(response.data);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
    }, [stock]);
    const addToCart = (productcode) => {
        const product = products.find(item => item.productcode === productcode);
        const productInCart = cart.find(item => item.productcode === productcode);
        const productInStock = stock.find(item => item.productcode === productcode);
        console.log('productcode :>> ', productcode);

        if (!productInStock || productInStock.stockQuantity === 0) {
            alert('Out of stock');
            return;
        }
        if (productInCart) {
            productInCart.stockQuantity += 1;
            setCart([...cart]);
        }
        else {
            setCart([...cart, { ...product, stockQuantity: 1 }]);
        }
        if (productInStock) {
            productInStock.stockQuantity -= 1;
            setStock([...stock]);

        }

    };

    const removeFromCart = (productcode) => {
        console.log('productcode :>> ', cart);
        console.log('productcode :>> ', stock);

        const updatedCart = cart.filter(item => {
            if (item.productcode === productcode) {
                item.stockQuantity -= 1;
            }
            return item.stockQuantity > 0;
        });

        const updatedStock = stock.filter(item => {
            if (item.productcode === productcode) {
                item.stockQuantity += 1
            }
            return item;
        })

        setCart(updatedCart);
        setStock(updatedStock);
    };

    const clearCart = () => {
        setCart([]);
        stock.forEach(stockItem => {
            const cartItem = cart.find(cartItem => cartItem.productcode === stockItem.productcode);
            if (cartItem) {
                stockItem.stockQuantity += cartItem.stockQuantity;
            }
        });

        setStock([...stock]);
    };

    const getTotal = () => {
        return cart.reduce((total, item) => total + item.priceunit * item.stockQuantity, 0).toFixed(2);
    };

    const checkout = async (cart) => {
        try {
            const response = await axios.post('https://localhost:7116/api/MasterProduct', cart, {});
            console.log(response.data);
            alert('Checkout completed');
            clearCart();
            GetMaster();
        } catch (error) {
            console.error(error);
        }
    };

    console.log('stock :>> ', stock);
    return (
        <div>
            <h4 style={{ textAlign: 'center', marginBottom: '20px' }}>Products</h4>
            <Grid container spacing={2}>
                {products.map(product => (
                    <Grid item xs={6} sm={4} key={product.productcode}>
                        <Card>
                            <CardContent>
                                <h3>{product.productname}</h3>
                                <p>Price: {product.priceunit} Baht</p>
                                <p>Stock: {product.stockQuantity}</p>
                                <Button
                                    variant="contained"
                                    color="primary"
                                    disabled={stock.find(item => item.productcode === product.productcode).stockQuantity === 0}
                                    onClick={() => addToCart(product.productcode)}
                                >
                                    Add to Cart
                                </Button>
                            </CardContent>
                        </Card>
                    </Grid>
                ))}
            </Grid>
            <br />
            <h4 style={{ textAlign: 'center', marginBottom: '20px' }}>Shopping Cart</h4>
            {cart.length > 0 ? (
                <div>
                    {cart.map(item => (
                        <div key={item.productcode}>
                            <p>
                                {item.productname}: {item.priceunit} x {item.stockQuantity}
                            </p>
                            <Button variant="contained" color="error" onClick={() => removeFromCart(item.productcode)}>
                                Remove
                            </Button>
                        </div>
                    ))}
                    <h6 style={{ textAlign: 'right', marginTop: '20px', fontSize: '15px' }}>Total: {getTotal()} Baht</h6>
                    <Button variant="contained" color="error" onClick={clearCart}>
                        Clear Cart
                    </Button>
                    <Button variant="contained" color="success" onClick={() => checkout(cart)}>
                        Check out
                    </Button>
                </div>
            ) : (
                <p style={{ textAlign: 'center' }}>Your cart is empty.</p>
            )}
        </div>

    );
};

export default Test;
